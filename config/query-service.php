<?php

/**
 * Config untuk query service
 */

return [
  "host_address" => env('QS_HOST_ADDRESS', '10.168.26.10'),
  "port" => env('QS_PORT', '23540'),
  "port_to" => env('QS_PORT_TO', '80'),
  "pan" => env('QS_PAN', '11000'),
];