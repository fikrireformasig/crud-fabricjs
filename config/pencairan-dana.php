<?php

/**
 * Config untuk menu pencairan-dana
 */

return [
  'flags' => [
    'success' => ['1'],
    'failed' => ['2'],
  ],

  'types' => [
    '1' => 'Manual',
    '2' => 'Otomatis',
  ]
];